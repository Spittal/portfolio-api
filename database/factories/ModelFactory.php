<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Jamie::class, function (Faker\Generator $faker) {
    return [
        'name' => 'Jamie Spittal',
        'position' => 'Lead Developer',
        'email' => 'contact@jamiespittal.com'
    ];
});

$factory->define(App\Work::class, function (Faker\Generator $faker) {
    $layouts = [
      'left',
      'center',
      'right'
    ];

    return [
        'title' => $faker->words(2),
        'url' => $faker->url,
        'description' => $faker->paragraph,
        'background' => $faker->hexcolor,
        'image' => $faker->imageUrl(800, 500, 'cats'),
        'layout' => rand(0,2)
    ];
});

$factory->define(App\Blog::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->words(2),
        'content' => $faker->paragraphs(3),
        'image' => $faker->imageUrl(1000, 500, 'cats')
    ];
});

$factory->define(App\Social::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word,
        'url' => $faker->url,
        'icon' => $faker->word
    ];
});

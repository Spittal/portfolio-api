FROM million12/nginx-php

WORKDIR /var/www/apps/api.jamiespittal.com
COPY ./ /var/www/apps/api.jamiespittal.com/
RUN composer install

# Small amount of custom config
COPY ./.docker/nginx.conf /etc/nginx/hosts.d/default.conf
COPY ./.docker/php-fpm-www-env.conf /data/conf/

RUN chown -R www:www /var/www/apps/api.jamiespittal.com
